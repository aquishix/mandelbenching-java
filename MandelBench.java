//package lada;

public class MandelBench {
    int pix_real=0;
    int pix_imag=0;
    int iteration=0;
    double z_real=0.0;
    double z_imag=0.0;
    double w_real=0.0;
    double w_imag=0.0;
    double c_real=0.0;
    double c_imag=0.0;
    String line="";
    boolean bailout=false;
    long timeSnap1 = 0;
    long timeSnap2 = 0;
    final int maxIterations = 1000000;
    
    long totalIterationCount = 0;
    
    public static void main(String[] args) {
    	MandelBench MB = new MandelBench();
        
    	MB.mandel();
    }
    
    void mandel() {
    	timeSnap1 = System.nanoTime();
    	for (pix_imag = 19; pix_imag >= -20; pix_imag--) {
    		c_imag = (double)pix_imag / 20;
    		line = "";
    		for (pix_real = -40; pix_real <= 39; pix_real++) {
    			bailout = false;
    			c_real = (double)pix_real / 40 - .5;
    			z_real = 0.0;
    			z_imag = 0.0;
    			for (iteration = 0; ((iteration < maxIterations) && !bailout); iteration++) {
    				w_real = (z_real * z_real) - (z_imag * z_imag) + c_real;
        			w_imag = (2 * z_real * z_imag) + c_imag;
        			z_real = w_real;
        			z_imag = w_imag;
        			if (z_real * z_real + z_imag * z_imag >= 4) {bailout = true;}
        			totalIterationCount++;
    			}
    			//line = line + computeCharacter(iteration);
    			System.out.print(computeCharacter(iteration));
    		}
    		//System.out.println(line);
    		System.out.println("  ");
    		//System.out.println("=(");
    	}
    	timeSnap2 = System.nanoTime();
    	System.out.println("Total iterations calculated: " + totalIterationCount);
    	System.out.println("Elapsed time: " + (((float)timeSnap2 - (float)timeSnap1)/1000000000) + " seconds.");
    }
   
    char computeCharacter(int dwell) {
    	if (dwell < (maxIterations/4)) {return 'O';}
    	if (((maxIterations/4) <= dwell) && (dwell < (maxIterations/2))) {return 'o';}
    	if (((maxIterations/2) <= dwell) && (dwell < (3*maxIterations/4))) {return '.';}
    	if (dwell >= (3*maxIterations/4)) {return ' ';}
    	return 'X';
    }
    
    /*
    boolean escaped(double a, double b) {
    	if
    }
    */
}
